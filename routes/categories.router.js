const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  res.json([
    {
      name: 'Salsas'
    },
    {
      name: 'Bebidas'
    }
  ])
});

module.exports = router;
