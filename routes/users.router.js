const express = require('express');
const { faker } = require('@faker-js/faker');

const router = express.Router();

router.get('/', (req, res) => {
  const users = [];
  for (let index = 0; index < 50; index++) {
    users.push({
      fullname: faker.person.fullName(),
      job: faker.person.jobTitle(),
      jobtype: faker.person.jobType()
    })

  }
  res.status(200).json(users)
});

module.exports = router;
