// Import express
const express = require('express');
const routerApi = require('./routes');
const { logErrors, errorHandler, boomErrorHandler } = require('./middlewares/error.handler');
const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const cors = require('cors');


// Create app
const app = express();

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'FakeStore api',
      version: '1.0.0',
    },
  },
  apis: ['./api-docs.js'],
};
const swaggerDocs = swaggerJSDoc(swaggerOptions);
const whitelist = ['http://localhost:8080', 'https://myapp.co'];
const options = {
  origin: (origin, callback) => {
    if (whitelist.includes(origin)) {
      callback(null, true);
    } else {
      callback(new Error('no permitido'));
    }
  }
}

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));
app.get('/swagger.json', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerDocs);
});

// Add port
const port = 3000;
app.use(express.json());
app.use(cors(options));


// New response
app.get('/', (req, res) => {
  res.send('Hola')
})


app.get('/categories/:id/products/:prodId', (req, res) => {
  res.json({
    name: 'Producto 2',
    price: 2000,
    id_category: req.params.id,
    id_product: req.params.prodId
  })
});

routerApi(app);
app.use(logErrors);
app.use(boomErrorHandler);
app.use(errorHandler);
// Listen port
app.listen(port, () => {
  console.log('My port ' + port)
})
